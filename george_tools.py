import sys
import numpy as np
from scipy.optimize import minimize
from scipy.linalg import LinAlgError
import george


def fit_gp(gp, y, yerr, par_range, nrandoms=1, quiet=False):
    """
    Fits gaussian process by optimizing its hyperparameters.
    Accepts a range of parameters where it draws nrandoms initial conditions to better avoid local minima.

    Parameters:
        gp: george.gp.GP object
            george gaussian process object
        y: np.ndarray
            observations to condition model on
        yerr: np.ndarray
            gaussian uncertainties of observations
        par_range: np.ndarray, shape (ndim, 2)
            limits, for each parameter, of region over which to sample random initial conditions. Follows order returned by gp.get_parameter_vector().
        nrandoms: int
            number of initial conditions to generate over par_range region.
        quiet: bool, defaults to False
            whether to print status messages or not

    Returns:
        none
    """
    ndim, _ = par_range.shape
    initial_conditions = np.random.rand(nrandoms, ndim) * (par_range[:,1] - par_range[:,0]) + par_range[:,0]

    def neg_ln_like(p):
        gp.set_parameter_vector(p)
        return -gp.log_likelihood(y)

    results = []
    funs = []
    if not quiet:
        print("Optimizing hyperparameters...")
    for i, initial_condition in enumerate(initial_conditions):
        if not quiet:
            print("{} initial condition...".format(i))
        gp.set_parameter_vector(initial_condition)
        try:
            result = minimize(neg_ln_like, gp.get_parameter_vector(), method="Nelder-Mead")
        except LinAlgError:
            print("WARNING: some initial condition resulted on ill-posed matrixes.", file=sys.stderr)
            continue
        if not quiet:
            print(result)
        results.append(result)
        funs.append(result.fun)

    result = results[np.argmin(funs)]
    if not quiet:
        print("Done")
        print(result)

    gp.set_parameter_vector(result.x)

